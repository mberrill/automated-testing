﻿using System.Collections.Generic;
using NorthwindTraders.Domain;

namespace NorthwindTraders.Application.Services
{
    public interface ICustomerPortfolio
    {
        void AddNewCustomer(Customer customer);
        IEnumerable<Customer> GetAll();
    }
}