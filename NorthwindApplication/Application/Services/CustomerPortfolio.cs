﻿using NorthwindTraders.Domain;
using NorthwindTraders.Persistance;
using System;
using System.Collections.Generic;

namespace NorthwindTraders.Application.Services
{
    public class CustomerPortfolio : ICustomerPortfolio
    {
        private readonly ICustomerStore _customerStore;

        public CustomerPortfolio(ICustomerStore customerStore)
        {
            _customerStore = customerStore;
        }

        public void AddNewCustomer(Customer customer)
        {
            var existingCustomer = _customerStore.GetCustomerById(customer.CustomerId);

            if (existingCustomer != null)
            {
                throw new DuplicateCustomerException("Customer already exists");
            }

            _customerStore.AddNewCustomer(customer);

            _customerStore.SaveCustomers();            
        }

        public IEnumerable<Customer> GetAll()
        {
            return _customerStore.GetAll();
        }
    }

    public class DuplicateCustomerException : Exception
    {
        public DuplicateCustomerException() { }
        public DuplicateCustomerException(string message): base(message) { }
    }
}

