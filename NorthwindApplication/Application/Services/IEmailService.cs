﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthwindTraders.Application.Services
{
    public interface IEmailService
    {
        void SendEmail();
    }

    public class EmailService : IEmailService
    {
        public void SendEmail()
        {
            //todo - send email here!
        }
    }
}
