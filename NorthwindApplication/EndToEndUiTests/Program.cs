﻿using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using System;
using System.Threading;

namespace EndToEndUiTests
{
    class Program
    {
        static IWebDriver driver = new EdgeDriver();
        static void Main(string[] args)
        {
            try // to run all tests
            {
                NavigateToCustomersPage_ShowsTableOfCustomers();
                ClickAddCustomer_AddsCustomerToEndOfList();

                // run more tests here...

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("PASSED");
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("FAILED");
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();

            driver.Dispose();
        }

        // This method tests that you can add a new customer via the web UI
        // But because we do not have control over the state it is difficult 
        // to make this repeatable because the entity id is 5 chars long
        // therefore end to end tests should either reset the state to a 
        // known state to allow for this before each test, remove any entities 
        // that are created after a test in order to reset the state, or not 
        // create entities at all.
        private static void ClickAddCustomer_AddsCustomerToEndOfList()
        {
            driver.Navigate().GoToUrl("http://localhost:5210/customers.html");

            var testCompanyId = "TEST6";

            driver.FindElement(By.Id("customerId")).SendKeys(testCompanyId);
            driver.FindElement(By.Id("customerName")).SendKeys("Test Company");
            
            driver.FindElement(By.Id("addNewButton")).Click();

            Thread.Sleep(200); // wait for Ajax call to complete

            var xpathToLastRowInTable = "//table//tbody//tr[last()]";

            var lastRowFirstCell = driver.FindElement(By.XPath(xpathToLastRowInTable + "/td"));

            if (lastRowFirstCell.Text != testCompanyId) throw new Exception("Test company not created");
        }

        public static void NavigateToCustomersPage_ShowsTableOfCustomers()
        {
            driver.Navigate().GoToUrl("http://localhost:5210/customers.html");

            var xpathToTableBodyRowsCells = "//table//tbody//tr//*";
            var tableBodyrowCells = driver.FindElements(By.XPath(xpathToTableBodyRowsCells));

            if (tableBodyrowCells.Count == 0) throw new Exception("No items in table of customers");
        }
    }
}
