﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;

using System.Threading;

namespace EndToEndUiTests
{
    [TestFixture]
    public class CustomersTests
    {
        private readonly IWebDriver driver;

        public CustomersTests()
        {
            driver = new EdgeDriver();
        }

        [Test]
        public void ClickAddCustomer_AddsCustomerToEndOfList()
        {
            driver.Navigate().GoToUrl("http://localhost:5210/customers.html");

            var testCompanyId = "11fg";

            driver.FindElement(By.Id("customerId")).SendKeys(testCompanyId);
            driver.FindElement(By.Id("customerName")).SendKeys("Test Company");

            driver.FindElement(By.Id("addNewButton")).Click();

            Thread.Sleep(1000); // wait for Ajax call to complete

            var xpathToLastRowInTable = "//table//tbody//tr[last()]";

            var lastRowFirstCell = driver.FindElement(By.XPath(xpathToLastRowInTable + "/td"));
                        
            Assert.That(lastRowFirstCell.Text == testCompanyId, "Test company not created");
        }

        [Test]
        public void NavigateToCustomersPage_ShowsTableOfCustomers()
        {
            driver.Navigate().GoToUrl("http://localhost:5210/customers.html");

            var xpathToTableBodyRowsCells = "//table//tbody//tr//*";
            var tableBodyrowCells = driver.FindElements(By.XPath(xpathToTableBodyRowsCells));

            Assert.That(tableBodyrowCells.Count > 0, "There should be items in table of customers");
        }

        [OneTimeTearDown]
        public void TearDownAfterAllTests()
        {
            driver.Dispose();
        }
    }
}
