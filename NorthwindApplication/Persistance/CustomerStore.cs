﻿using NorthwindTraders.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NorthwindTraders.Persistance
{
    public class CustomerStore : ICustomerStore
    {
        private readonly NorthwindContext _context;

        public CustomerStore(NorthwindContext context)
        {
            _context = context;
        }

        public IEnumerable<Customer> GetAll()
        {
            return _context.Customers;
        }

        public void AddNewCustomer(Customer customer)
        {
            _context.Customers.Add(customer);
        }

        public Customer GetCustomerById(string customerId)
        {
            return _context.Customers.Find(customerId);
        }

        public async Task SaveCustomers()
        {
            await _context.SaveChangesAsync();
        }
    }
}
