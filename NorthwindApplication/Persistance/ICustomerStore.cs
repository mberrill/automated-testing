﻿using System.Threading.Tasks;
using NorthwindTraders.Domain;
using System.Collections.Generic;

namespace NorthwindTraders.Persistance
{
    public interface ICustomerStore
    {
        IEnumerable<Customer> GetAll();
        void AddNewCustomer(Customer customer);
        Task SaveCustomers();
        Customer GetCustomerById(string customerId);
    }
}