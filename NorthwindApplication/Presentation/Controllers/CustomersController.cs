using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NorthwindTraders.Application.Customers.Queries.GetCustomersList;
using NorthwindTraders.Application.Customers.Commands.CreateCustomer;
using NorthwindTraders.Application.Services;
using NorthwindTraders.Domain;
using System;
using System.Linq;

namespace NorthwindTraders.Controllers
{
    [Produces("application/json")]
    [Route("api/Customers")]
    public class CustomersController : Controller
    {
        public readonly ICustomerPortfolio _customerPortfolio;

        public CustomersController(ICustomerPortfolio customerPortfolio)
        {            
            _customerPortfolio = customerPortfolio;
        }

        // GET api/customers
        [HttpGet]
        public IEnumerable<CustomerListModel> Get()
        {
            return _customerPortfolio.GetAll().Select(customer => new CustomerListModel()
            {
                Id = customer.CustomerId,
                Name = customer.CompanyName
            });
        }

        // POST api/customers
        [HttpPost]
        public IActionResult Create([FromBody]CreateCustomerModel createCustomerModel)
        {
            // check that the request was valid
            if (createCustomerModel == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // build the domain object required to add a new customer
            var customer = new Customer
            {
                CustomerId = createCustomerModel.Id,
                CompanyName = createCustomerModel.CompanyName,
            };

            
            try
            {
                _customerPortfolio.AddNewCustomer(customer);
            }
            catch(DuplicateCustomerException)
            {
                return StatusCode(409, "Customer already exists");
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Created("api/customer", customer);
        }
    }
}