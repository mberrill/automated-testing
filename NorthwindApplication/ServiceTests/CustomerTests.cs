﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using NorthwindTraders;
using NUnit.Framework;
using System.Net.Http;
using System.Threading.Tasks;

namespace ServiceTests
{
    [TestFixture]
    public class CustomerTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        public CustomerTests()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            _client = _server.CreateClient();
        }
        [Test]
        public async Task CreateNewCustomer_CreatesCustomerAsync()
        {
            var response = await _client.GetAsync("/api/customers");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            
        }
    }
}
