﻿using NUnit.Framework;
using System;
using System.Linq;

namespace UnitTests
{
    public class SampleCodeExample
    {
        public int ParseAndSum(string numbersText)
        {
            if (numbersText == null || numbersText.Length == 0)
                return 0;

            var numbers = numbersText.Split(',');
            var parsedNumbers = numbers.Select(x => int.Parse(x));
            return parsedNumbers.Sum();
        }
    }


    [TestFixture]
    public class ExampleUnitTest
    {
        [Test]
        public void ParseAndSum_withValidText_SumsNumbers()
        {
            SampleCodeExample sut = new SampleCodeExample();
            var sum = sut.ParseAndSum("1,2,3");
            Assert.That(sum == 6);
        }

        [Test]
        public void ParseAndSum_withEmptyText_ReturnsZero()
        {
            SampleCodeExample sut = new SampleCodeExample();
            var sum = sut.ParseAndSum("");
            Assert.That(sum == 0);
        }
        [Test]
        public void ParseAndSum_withNullText_ReturnsZero()
        {
            SampleCodeExample sut = new SampleCodeExample();
            var sum = sut.ParseAndSum(null);
            Assert.That(sum == 0);
        }       

        [Test]
        public void ParseAndSum_withInValidText_SumsNumbers()
        {
            SampleCodeExample sut = new SampleCodeExample();
            Assert.Throws<FormatException>(() => sut.ParseAndSum("adsdasfdsg"));            
        }
    }
}
