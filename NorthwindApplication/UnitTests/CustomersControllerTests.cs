﻿using NorthwindTraders.Application.Services;
using NorthwindTraders.Controllers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using NorthwindTraders.Domain;
using System.Linq;
using NorthwindTraders.Application.Customers.Commands.CreateCustomer;
using Microsoft.AspNetCore.Mvc;

namespace UnitTests
{
    [TestFixture]
    public class CustomersControllerTests
    {
        [Test]
        public void Get_WhenNoCustomers_Returns200WithEmptyList()
        {
            CustomersController sut = new CustomersController(new CustomerPortfolioWithNoCustomersStub());
            var customers = sut.Get();
            Assert.That(customers.Count() == 0);
        }

        [Test]
        public void Create_WithValidCustomerButAlreadyExists_Returns409Conflict()
        {
            // we are not testing the logic of how to detect a duplicate
            // we are testing that the http output is as expected when a 
            // duplicate exception is thrown - We are only testing what the controller is repsonsible for

            CustomersController sut = new CustomersController(new CustomerPortfolioSimulateDuplicateStub());
            CreateCustomerModel customerToAdd = new CreateCustomerModel()
            {
                Id = "1234",
                CompanyName = "Company Name"
            };
            var result = (ObjectResult)sut.Create(customerToAdd);
            Assert.That(result.StatusCode == 409);
        }
    }

    public class CustomerPortfolioWithNoCustomersStub : ICustomerPortfolio
    {
        public void AddNewCustomer(Customer customer)
        {
            // do nothing
        }

        public IEnumerable<Customer> GetAll()
        {
            return new Customer[] { };
        }
    }

    public class CustomerPortfolioSimulateDuplicateStub : ICustomerPortfolio
    {
        public void AddNewCustomer(Customer customer)
        {
            throw new DuplicateCustomerException("This is a fake exception that simulates that the customer is a duplicate");
        }

        public IEnumerable<Customer> GetAll()
        {
            return new Customer[] { };
        }
    }
}
