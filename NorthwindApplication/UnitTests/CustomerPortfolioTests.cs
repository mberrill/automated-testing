﻿using NorthwindTraders.Application.Services;
using NUnit.Framework;
using NorthwindTraders.Domain;
using Moq;
using NorthwindTraders.Persistance;

namespace UnitTests
{
    [TestFixture]
    public class CustomerPortfolioTests
    {
        private Mock<ICustomerStore> customerStoreMock;
        private CustomerPortfolio sut;

        [SetUp]
        public void Setup()
        {
            customerStoreMock = new Mock<ICustomerStore>();
            sut = new CustomerPortfolio(customerStoreMock.Object);
        }


        [Test]
        public void AddNewCustomer_WithValidCustomer_SavesNewCustomer()
        {
            // we need to do behaviour testing here so make sure we invoke the SaveCustomers method on the store

            Customer customerToAdd = new Customer()
            {
                CustomerId = "1234",
                CompanyName = "Company Name"
            };

            sut.AddNewCustomer(customerToAdd);
                        
            customerStoreMock.Verify(x => x.SaveCustomers(), Times.Once, "SaveCustomers was not called as expected");
        }

        [Test] // this is the same test as above but asserts by checking there was no exception thrown
        public void AddNewCustomer_WithValidCustomer_DoesNotThrow()
        {
            Customer customerToAdd = new Customer()
            {
                CustomerId = "1234",
                CompanyName = "Company Name"
            };

            Assert.DoesNotThrow(() => sut.AddNewCustomer(customerToAdd));            
        }

        [Test]
        public void AddNewCustomer_WhenCustomerAlreadyExists_ThrowsDuplicateCustomerException()
        {
            Customer customerToAdd = new Customer()
            {
                CustomerId = "1234",
                CompanyName = "Company Name"
            };

            customerStoreMock.Setup(x => x.GetCustomerById("1234")).Returns(customerToAdd); // we simulate that it already exists!

            Assert.Throws<DuplicateCustomerException>(()=> sut.AddNewCustomer(customerToAdd), "We should throw DuplicateCustomerException if the customer already exists");
        }



    }
}