﻿using NorthwindTraders.Application.Services;
using NorthwindTraders.Controllers;
using NUnit.Framework;
using NorthwindTraders.Domain;
using System.Linq;
using Moq;
using NorthwindTraders.Application.Customers.Commands.CreateCustomer;
using Microsoft.AspNetCore.Mvc;

namespace UnitTests
{
    [TestFixture]
    public class CustomersControllerTestsWithMoq
    {
        [Test]
        public void Get_WhenNoCustomers_ReturnsEmptyList()
        {
            // create the stub (BTW it is not a mock because we will not use it to verfiy the call to GetAll)
            Mock<ICustomerPortfolio> customerPortfolioStub = new Mock<ICustomerPortfolio>();

            customerPortfolioStub.Setup(x => x.GetAll()).Returns(new Customer[0]);

            CustomersController sut = new CustomersController(customerPortfolioStub.Object);
            var customers = sut.Get();
            Assert.That(customers.Count() == 0);
        }


        [Test]
        public void Create_WithValidCustomerButAlreadyExists_Returns409Conflict()
        {
            Mock<ICustomerPortfolio> customerPortfolioStub = new Mock<ICustomerPortfolio>();

            CustomersController sut = new CustomersController(customerPortfolioStub.Object);

            CreateCustomerModel customerToAdd = new CreateCustomerModel()
            {
                Id = "1234",
                CompanyName = "Company Name"
            };

            // simulate a duplicate customer response
            customerPortfolioStub.Setup(x => x.AddNewCustomer(It.IsAny<Customer>())).Throws<DuplicateCustomerException>();

            var result = (ObjectResult)sut.Create(customerToAdd);
            Assert.That(result.StatusCode == 409);
        }
    }    
}