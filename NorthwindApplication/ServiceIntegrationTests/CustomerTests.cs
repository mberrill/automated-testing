﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NorthwindTraders;
using NUnit.Framework;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NorthwindTraders.Persistance;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Linq;
using NorthwindTraders.Application.Customers.Queries.GetCustomersList;

namespace ServiceTests
{
    [TestFixture]
    public class CustomerTests
    {
        private  TestServer _server;
        private  HttpClient _client;


        [SetUp]
        public void SetupBeforeEachTest()
        {
            // Use the real Startup class, but set the DB context to use an in-memory DB instead so that
            // the state of the application is known before each test
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>().ConfigureServices((services) => {
                services.AddDbContext<NorthwindContext>(options => options.UseInMemoryDatabase("Test"));
            }));

            _client = _server.CreateClient();
        }
        

        [Test]
        public async Task GetCustomers_ReturnsArrayOfCustomers()
        {
            //ARRANGE - nothing to do here

            //ACT
            var response = await _client.GetAsync("/api/customers");

            //ASSERT
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            var allCustomers = JsonConvert.DeserializeObject<JArray>(responseString);

            Assert.That(allCustomers.Count > 0);
        } 

        [Test]
        public async Task AddCustomerPost_AddsNewCustomer()
        {
            var content = new StringContent("{'Id': '1234', 'CompanyName' : 'abcd'}", Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("/api/customers", content);

            response.EnsureSuccessStatusCode();

            var responseGetAll = await _client.GetAsync("/api/customers");

            var responseString = await responseGetAll.Content.ReadAsStringAsync();

            // we can use the real CustomerListModel from the api to assert that it works because we have a reference to the project
            var allCustomers = JsonConvert.DeserializeObject< CustomerListModel[]>(responseString);

            Assert.That(allCustomers.Any(x => x.Id == "1234"));
        }

        // we could test for all sorts of tests here, but the tests take a while to run, 
        // and also they could ran at the unit test level just as affectively.

        [Test]
        public async Task AddCustomerPost_CustomerAlreadyExists_ReturnsConflct()
        {
            var content = new StringContent("{'Id': '1234', 'CompanyName' : 'abcd'}", Encoding.UTF8, "application/json");

            var responseToFirstAdd = _client.PostAsync("/api/customers", content).Result;
            
            var responseToSecondAdd = await _client.PostAsync("/api/customers", content);
            Assert.That(responseToSecondAdd.StatusCode == System.Net.HttpStatusCode.Conflict);
        }

        


        [TearDown]
        public void TearDownAfterEachTest()
        {
            _server.Dispose();
            _client.Dispose();
        }
    }
}